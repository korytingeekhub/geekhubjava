package worktask;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EditDate {

    public static boolean dateNowYesNo(Date date){
        SimpleDateFormat formatDate = new SimpleDateFormat("dd.MM.yyyy");
        Date dateNow = new Date();
        String stringDate = formatDate.format(date);
        String stringDateNow = formatDate.format(dateNow);
        return stringDate.equals(stringDateNow);
        }

    public static String dateToString(Date date){
        SimpleDateFormat formatDate = new SimpleDateFormat("dd.MM.yyyy");
        return formatDate.format(date);
    }

    public static String datetimeToString(Date date){
        SimpleDateFormat formatDate = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        return formatDate.format(date);
    }

    public static Date stringToDateTime(String stringDate){
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        try {
            return sdf.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}

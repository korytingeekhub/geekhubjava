package worktask;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static worktask.EditDate.*;

public class DemoWorkTaskManager {

    public static void main(String[] args) {
	// write your code here

        TaskManager demoTaskManager = new CollectionTask();

        createTimeItem(demoTaskManager);

        System.out.println();
        System.out.println("**** Категория Дом ****");
        printlnListTask(demoTaskManager.getTasksByCategory("Дом"),demoTaskManager);

        System.out.println();
        System.out.println("**** Все категории ****");
        printCollection(demoTaskManager.getCategories());

        System.out.println();
        System.out.println("**** Планы на сегодня ****");
        printlnListTask(demoTaskManager.getTasksForToday(),demoTaskManager);

        queryCollectionItem(demoTaskManager,dateToString(new Date())+" 11:00",null,TypQueryCollection.REMOVE);
        System.out.println();
        System.out.println("**** Удалили задачу "+dateToString(new Date())+" в 11:00 ****");
        printlnListTask(demoTaskManager.getTasksForToday(),demoTaskManager);

        System.out.println();
        System.out.println("**** Все категории с планами ****");
        printMapCollection(demoTaskManager.getTasksByCategories(),demoTaskManager);

    }

    private static void queryCollectionItem(TaskManager taskManager, String date, Task task,TypQueryCollection typ){
            if (typ == TypQueryCollection.ADD) taskManager.addTask(stringToDateTime(date), task );
            if (typ == TypQueryCollection.REMOVE) taskManager.removeTask(stringToDateTime(date));
    }

    private static void printlnListTask(List<Task> inList, TaskManager demoTaskManager){
        for(Task task : inList){
            System.out.println(datetimeToString(demoTaskManager.getDateTask(task))+" "+task.getDescription());
        }
    }

    private static void printCollection(Collection<String> inList){
        for(String string : inList){
            System.out.println(string);
        }
    }

    private static void createTimeItem(TaskManager taskManager){

        Date tempDate = new Date();
        addItemToCollection(taskManager,dateToString(tempDate));
        tempDate = new Date(System.currentTimeMillis() - (3600 * 1000 * 24));
        addItemToCollection(taskManager,dateToString(tempDate));
        tempDate = new Date(System.currentTimeMillis() + (3600 * 1000 * 24));
        addItemToCollection(taskManager,dateToString(tempDate));

    }


    private static void addItemToCollection(TaskManager taskManager, String date){

        Task task = new Task("Дом", "Умыться");
        queryCollectionItem(taskManager, date+" 07:10", task, TypQueryCollection.ADD);
        task = new Task("Дом", "Завтракать");
        queryCollectionItem(taskManager, date+" 07:30", task, TypQueryCollection.ADD);
        task = new Task("Дом", "Проснуться");
        queryCollectionItem(taskManager, date+" 07:00", task, TypQueryCollection.ADD);

        task = new Task("Работа", "Планерка");
        queryCollectionItem(taskManager, date+" 09:00", task, TypQueryCollection.ADD);
        task = new Task("Работа", "Провести встречу");
        queryCollectionItem(taskManager, date+" 11:00", task, TypQueryCollection.ADD);
        task = new Task("Работа", "Встреча с кандидатом");
        queryCollectionItem(taskManager, date+" 16:00", task, TypQueryCollection.ADD);

    }

    private static void printMapCollection(Map<String, List<Task>> inMapCollection,  TaskManager demoTaskManager){
        for (String categories : inMapCollection.keySet()){
            System.out.println("");
            System.out.println("**** Категория "+categories+" ****");
            List<Task> tempTaskList = inMapCollection.get(categories);
            for (Task task : tempTaskList){
                System.out.println(datetimeToString(demoTaskManager.getDateTask(task))+" "+task.getDescription());
            }
        }
    }
}



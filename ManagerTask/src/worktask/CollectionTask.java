package worktask;

import java.util.*;

public class CollectionTask implements TaskManager {

    private Map<Date,Task> collectionTask = new TreeMap<>();

    @Override
    public void addTask(Date date, Task task) {
        collectionTask.put(date,task);
    }

    @Override
    public void removeTask(Date date) {
        collectionTask.remove(date);
    }

    @Override
    public Collection<String> getCategories() {
        Set<String> tempList = new LinkedHashSet<>();
        for (Task task : collectionTask.values()){
            tempList.add(task.getCategory());
        }
        return tempList;
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories() {
        Set<String> categories = new LinkedHashSet<>(getCategories());
        Map<String, List<Task>> tempMap = new LinkedHashMap<>();
        for (String category : categories){
            tempMap.put(category,getTasksByCategory(category));
        }
        return tempMap;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        List<Task> returnList = new ArrayList<>();
        for(Task taskCollection : collectionTask.values()){
            if (taskCollection.getCategory().equals(category)){
                    returnList.add(taskCollection);
            }
        }

        return returnList;
    }

    @Override
    public List<Task> getTasksForToday() {
        List<Task> tempList = new ArrayList<>();
        for (Date date :collectionTask.keySet()){
            if (EditDate.dateNowYesNo(date)){
                tempList.add(collectionTask.get(date));
            }
        }
        return tempList;
    }

    @Override
    public Date getDateTask(Task task){
        for (Date date : collectionTask.keySet())
        {
            if (task.equals(collectionTask.get(date))) return date;
        }
        return null;
    }



}


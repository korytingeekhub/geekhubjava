package worktask;

import java.util.*;

public class Task {

    private String category;
    private String description;

    public String getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }


    public Task (String inCategory, String inDescription) {
        category = inCategory;
        description = inDescription;
    }



}

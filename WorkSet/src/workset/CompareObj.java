package workset;

import java.util.Set;

public class CompareObj {

    public static boolean typSetObject(Set a, Set b){
        Object typObjA = getTypObject(a);
        Object typObjB = getTypObject(b);
        if (typObjA == null || typObjB == null){
            System.out.println("Error. Set is empty");
            return false;
        }
        return typObjA.getClass() == typObjB.getClass();
    }

    private static Object getTypObject(Set a){
        for(Object objA : a){
            return objA;
        }
        return null;
    }
}

package workset;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetRelations implements SetOperations {

    @Override
    public boolean equals(Set a, Set b) {
        if (!(CompareObj.typSetObject(a,b))) {
            return false;
        }
        if (a.size() != b.size()){
            return false;
        }
        for (Object objSet : b ){
            if (!(a.contains(objSet))){
                return false;
            }
        }
        return true;
    }

    @Override
    public Set union(Set a, Set b) {
        Set tempSet = new LinkedHashSet();
        if (CompareObj.typSetObject(a,b)){
            tempSet.addAll(a);
            tempSet.addAll(b);
        }else {
            System.out.println("Set typ error");
        }
        return tempSet;
    }

    @Override
    public Set subtract(Set a, Set b) {
        Set tempSet = new TreeSet();
        if (CompareObj.typSetObject(a,b)){
            tempSet.addAll(a);
            tempSet.removeAll(b);
        }else {
            System.out.println("Set typ error");
        }
        return tempSet;
    }

    @Override
    public Set intersect(Set a, Set b) {
        Set tempSet = new LinkedHashSet();
        if (CompareObj.typSetObject(a,b)){
            for (Object objValue : b ){
                if (a.contains(objValue)){
                    tempSet.add(objValue);
                }
            }
        }else {
            System.out.println("Set typ error");
        }
        return tempSet;
    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {
        Set tempSet = new TreeSet();
        if (CompareObj.typSetObject(a,b)){
            tempSet.addAll(a);
            tempSet.addAll(b);
            for (Object objValue : b ){
                if (a.contains(objValue)){
                    tempSet.remove(objValue);
                }
            }
        } else {
            System.out.println("Set typ error");
        }
        return tempSet;
    }

}

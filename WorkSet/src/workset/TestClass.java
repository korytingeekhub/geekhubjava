package workset;

import java.util.HashSet;
import java.util.Set;

public class TestClass {

    public static void main(String[] args) {
	// write your code here
        Set<String> newSet = new HashSet<>();
        Set<String> newSet2 = new HashSet<>();

        newSet.add("1");
        newSet.add("2");
        newSet.add("3");

        newSet2.add("4");
        newSet2.add("2");
        newSet2.add("6");

        SetOperations newRealt = new SetRelations();
        System.out.println("****** method equals()  ******");
        System.out.println(newRealt.equals(newSet,newSet2));
        System.out.println("****** method union()  ******");
        System.out.println(newRealt.union(newSet,newSet2));
        System.out.println("****** method subtract()  ******");
        System.out.println(newRealt.subtract(newSet,newSet2));
        System.out.println("****** method intersect()  ******");
        System.out.println(newRealt.intersect(newSet,newSet2));
        System.out.println("****** method symmetricSubtract()  ******");
        System.out.println(newRealt.symmetricSubtract(newSet,newSet2));


    }
}

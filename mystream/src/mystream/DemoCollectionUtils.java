package mystream;

public class DemoCollectionUtils {

    public static void main(String[] args) {

        CollectionEmployees newCollectEmpl = new CollectionEmployees();

        newCollectEmpl.addDepatment("Java Developer",10);
        newCollectEmpl.addDepatment("Administrators",4);
        newCollectEmpl.addDepatment("Bookkeeping", 2);

        newCollectEmpl.getString();
        System.out.println("********************************");
        newCollectEmpl.filterDepatment("Book");
        System.out.println("********************************");
        newCollectEmpl.depatmentContains("Book",TypContains.ANY);
        newCollectEmpl.depatmentContains("Book",TypContains.ALL);
        newCollectEmpl.depatmentContains("grot",TypContains.NONE);
        System.out.println("********************************");
        newCollectEmpl.lengthDepatment();
        System.out.println("********************************");
        System.out.println("**** MAX ****");
        newCollectEmpl.minMaxSurname("Java Developer",true);
        System.out.println("**** MIN ****");
        newCollectEmpl.minMaxSurname("Java Developer",false);
        newCollectEmpl.printName("Java Developer");
        System.out.println("********************************");
        newCollectEmpl.printSurname("Administrators");
        System.out.println("********************************");
        newCollectEmpl.moreElevenCharToDepatment();
        System.out.println("********************************");
        newCollectEmpl.yearBirthdateEmpl("Java Developer");
        System.out.println("********************************");
        System.out.println("****  Increase of wages Java Developer ****");
        System.out.println("********************************");
        newCollectEmpl.employeesDoublSalary("Java Developer");

    }
}

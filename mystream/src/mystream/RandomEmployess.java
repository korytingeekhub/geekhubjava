package mystream;
import java.time.LocalDate;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class RandomEmployess {

    private Employees employees;

    public Employees getEmployees() {
        return employees;
    }

    public RandomEmployess(){
      employees = new Employees(outRandomString(15),outRandomString(10),outRandomLocalDate(), randomSalary());
    }

    public static String outRandomString(int charCount){
        String outString = "";
        outString += randomChar(true);
        for (int i = 0; i<charCount; i++){
            outString += randomChar(false);
        }
        return outString;
    }

    private static char randomChar(boolean UpOrDownChar){
        Random rnd = new Random();
        int caseUpOrDown;
        if (UpOrDownChar) caseUpOrDown = 66;
        else caseUpOrDown = 98;

        return (char)(rnd.nextInt(25)+caseUpOrDown);
    }

    private static LocalDate outRandomLocalDate(){
        long minDay = LocalDate.of(1970, 1, 1).toEpochDay();
        long maxDay = LocalDate.of(1995, 12, 31).toEpochDay();
        long randomDay = ThreadLocalRandom.current().nextLong(minDay, maxDay);
        return LocalDate.ofEpochDay(randomDay);
    }

    private static Double randomSalary(){

        Double randomDouble = new Random().nextDouble();
        Integer randomInt = new Random().nextInt(700)+300;
        randomDouble =randomDouble+randomInt;
        return  randomDouble;
    }


}

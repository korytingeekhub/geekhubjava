package mystream;

import java.time.LocalDate;

public class Employees {

    private String surname;
    private String name;
    private LocalDate birthday;
    private Double salary;

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public Double getSalary() {
        return salary;
    }

    public Employees(String surname, String name, LocalDate birtday, Double salary){
        this.surname = surname;
        this.name = name;
        this.birthday = birtday;
        this.salary = salary;
    }

    public Employees(){
        Employees randomEmployees = new RandomEmployess().getEmployees();
        this.surname = randomEmployees.surname;
        this.name = randomEmployees.name;
        this.birthday = randomEmployees.birthday;
        this.salary = randomEmployees.salary;
    }

    public void getString(){

        System.out.println(this.toString());
    }

    public int compare(Employees employees){
        return this.birthday.compareTo(employees.birthday);
    }

    public String toString(){
        return "Surname: "+surname+", Name: "+name+", Birthday: "+birthday+" Salary = "+String.format("%.2f",salary)+" $";
    }
}

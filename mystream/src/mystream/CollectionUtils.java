package mystream;

import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.*;
import java.util.function.*;

public class CollectionUtils {

    public static <E> List<E> generate(Supplier<E> generator, int count) {
        if (count < 1) return null;

        List<E> returnList = new ArrayList<>();
        for (int i = 0; i < count; i++){
            returnList.add(generator.get());
        }
        return returnList;
    }

    public static <E> List<E> filter(List<E> elements, Predicate<E> filter) {
        if (elements.size() < 1){
            return null;
        }

        List<E> returnList = new ArrayList<>();
        for(E obj : elements){
            if (filter.test(obj)){
                returnList.add(obj);
            }
        }
        return returnList;
    }

    public static <E> boolean anyMatch(List<E> elements, Predicate<E> predicate) {
        if (elements.size() < 1){
            return false;
        }
        for(E obj : elements){
            if (predicate.test(obj)){
                return true;
            }
        }
        return false;
    }

    public static <E> boolean allMatch(List<E> elements, Predicate<E> predicate) {
        if (elements.size() < 1){
            return false;
        }
        for(E obj : elements){
            if (predicate.negate().test(obj)){
                return false;
            }
        }
        return true;

    }

    public static <E> boolean noneMatch(List<E> elements, Predicate<E> predicate) {
        if (elements.size() < 1){
            return true;
        }
        for(E obj : elements){
            if (predicate.test(obj)){
                return false;
            }
        }
        return true;
    }

    public static <T, R> List<R> map(List<T> elements, Function<T, R> C) {
        if (elements.size() < 1){
            return null;
        }

        List<R> returnList = new ArrayList<>();
        for(T obj : elements){
            returnList.add(C.apply(obj));
        }
        return returnList;
    }

    public static <E> Optional<E> max(List<E> elements, Comparator<E> comparator) {

        Optional<E> maxElements =  Optional.ofNullable(elements.get(0));
        for(E obj : elements){
            if (comparator.compare(maxElements.get(), obj) < 0){
                maxElements = Optional.ofNullable(obj);
            }
        }
        return maxElements;
    }

    public static <E> Optional<E> min(List<E> elements, Comparator<E> comparator) {

        Optional<E> minElements =  Optional.ofNullable(elements.get(0));
        for(E obj : elements){
            if (comparator.compare(minElements.get(), obj) > 0){
                minElements = Optional.ofNullable(obj);
            }
        }
        return minElements;
    }

    public static <E> List<E> distinct(List<E> elements) {
        Set<E> retuenSet = new HashSet<E>(elements);
        return new ArrayList<>(retuenSet);
    }

    public static <E> void forEach(List<E> elements, Consumer<E> consumer) {
        for (E element : elements){
            consumer.accept(element);
        }
    }

    public static <E> Optional<E> reduce(List<E> elements, BinaryOperator<E> accumulator) {
        Optional<E> returnAccum = Optional.ofNullable(elements.get(0));
        for (int i = 1; i <elements.size();i++){
            returnAccum = Optional.ofNullable(accumulator.apply(returnAccum.get(),elements.get(i)));
        }
        return returnAccum;
    }

    public static <E> E reduce(E seed, List<E> elements, BinaryOperator<E> accumulator) {
        E returnAccum = accumulator.apply(seed, elements.get(0));;
        for (int i = 1; i <elements.size();i++){
            E sumElements = accumulator.apply(seed,elements.get(i));
            returnAccum = accumulator.apply(returnAccum,sumElements);
        }
        return returnAccum;
    }

    public static <E> Map<Boolean, List<E>> partitionBy(List<E> elements, Predicate<E> predicate) {
        Map<Boolean, List<E>> returnMap = new HashMap<>();
        for (E elemetn : elements){
             List<E> tempList =returnMap.get(predicate.test(elemetn));
             if (tempList == null){
                 tempList = new LinkedList<>();
             }
             tempList.add(elemetn);
             returnMap.put(predicate.test(elemetn),tempList);
        }
        return returnMap;
    }

    public static <T, K> Map<K, List<T>> groupBy(List<T> elements, Function<T, K> classifier) {
        Map<K, List<T>> returnMap = new TreeMap<>();

        for (T elemetn : elements){
            List<T> tempList =returnMap.get(classifier.apply(elemetn));
            if (tempList == null){
                tempList = new LinkedList<>();
            }
            tempList.add(elemetn);
            returnMap.put(classifier.apply(elemetn),tempList);
        }

        return returnMap;
    }

    public static <T, K, U> Map<K, U> toMap(List<T> elements,
                                            Function<T, K> keyFunction,
                                            Function<T, U> valueFunction,
                                            BinaryOperator<U> mergeFunction) {
       Map<K, U> returnMap = new TreeMap<>();
       K key;
       U value;
       for (T obj : elements){
           key = keyFunction.apply(obj);
           value = valueFunction.apply(obj);
           value = mergeFunction.apply(value,value);
           returnMap.put(key,value);
       }
       return returnMap;
    }
}



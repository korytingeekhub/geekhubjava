package mystream;

import java.time.LocalDate;
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class CollectionEmployees extends CollectionUtils {

    private Map<String, List<Employees>> collectionEmployess = new TreeMap<>();

    public void getString(){
        for (String depatment : collectionEmployess.keySet()){
            System.out.println("");
            System.out.println("**** "+depatment+" ****");
            List<Employees> tempEmployeesList = collectionEmployess.get(depatment);
            for (Employees employees : tempEmployeesList){
                employees.getString();
            }
        }
    }

    private void printlnListDepatment(List inList){
        for(Object depatment : inList){
            System.out.println(depatment);
        }
    }

    private <T,E> void printMap(Map<T,List<E>> map){
        for (Object key : map.keySet()){
            System.out.println("");
            System.out.println("**** "+key+" ****");
            List tempList = map.get(key);
            for (Object obj : tempList){
                System.out.println(obj.toString());
            }
        }
    }

    private <T,E> void printMapSalary(Map<T,E> map){
        for (Object key : map.keySet()){
            System.out.println("**** "+key+" ****");
            System.out.println(String.format("%.2f",map.get(key))+" $");
        }
    }

    public void addDepatment(String depatment,int countEmployees){
        collectionEmployess.put(depatment,generate(Employees::new,countEmployees));
    }

    public void filterDepatment(String depatment){
        List<String> keyList = new LinkedList<>();
        keyList.addAll(collectionEmployess.keySet());
        Predicate<String> containsLetter = p -> p.contains(depatment);
        List tempList =  filter(keyList, containsLetter );
        printlnListDepatment(tempList);
    }

    public void depatmentContains(String depatment, TypContains typContains){
        List<String> keyList = new LinkedList<>();
        keyList.addAll(collectionEmployess.keySet());
        Predicate<String> containsLetter = p -> p.contains(depatment);
        switch (typContains){
            case ALL:
                System.out.println("ALL");
                System.out.println(allMatch(keyList,containsLetter));
                break;
            case ANY:
                System.out.println("ANY");
                System.out.println(anyMatch(keyList,containsLetter));
                break;
            case NONE:
                System.out.println("NONE");
                System.out.println(noneMatch(keyList,containsLetter));
                break;
        }
    }

    public void lengthDepatment(){
        List<String> keyList = new LinkedList<>();
        keyList.addAll(collectionEmployess.keySet());
        Function<String,Integer> lengthLetter = s -> s.length();
        printlnListDepatment(map(keyList,lengthLetter));

    }

    public void minMaxSurname(String depatment, boolean Max){
        List<Employees> employeesList = collectionEmployess.get(depatment);
        Optional<Employees> printEmployees;
        if (Max){
            printEmployees = max(employeesList, (a,b) -> a.compare(b));
        } else {
            printEmployees = min(employeesList, (a,b) -> a.compare(b));
        }
        printEmployees.get().getString();
    }

    public void printName(String depatment){
        List<Employees> employeesList = collectionEmployess.get(depatment);
        System.out.println("Depatment: "+depatment);
        Consumer<Employees> printNames = (p) -> System.out.println("Employees name: " + p.getName());
        forEach(employeesList,printNames);

    }

    public void printSurname(String depatment){
        List<Employees> employeesList = collectionEmployess.get(depatment);
        List<String> surnameList = new LinkedList<>();
        for (Employees employees :employeesList){
            surnameList.add(employees.getSurname());
        }
        System.out.println("Depatment: "+depatment);
        BinaryOperator<String> printSurnames = (e,n) -> (e+" "+n);
        System.out.println("*****  Optional *****");
        System.out.println(reduce(surnameList,printSurnames));
        System.out.println("*****  Optional *****");
        System.out.println(reduce("Surname",surnameList,printSurnames));
    }

    public void moreElevenCharToDepatment(){
        List<String> keyList = new LinkedList<>();
        keyList.addAll(collectionEmployess.keySet());
        Predicate<String> containsLetter = p -> p.length() > 11;
        printMap(partitionBy(keyList,containsLetter));
    }

    public void yearBirthdateEmpl(String depatment){
        List<Employees> employeesList = collectionEmployess.get(depatment);
        Function<Employees,Integer> yearBirthday = e -> e.getBirthday().getYear();
        printMap(groupBy(employeesList,yearBirthday));
    }

    public void employeesDoublSalary(String depatment){
        List<Employees> employeesList = collectionEmployess.get(depatment);
        Function<Employees,String> surname = e -> e.getSurname();
        Function<Employees,Double> salary = e -> e.getSalary();
        BinaryOperator<Double> doublSalare = (a,b) -> a + b;
        printMapSalary(toMap(employeesList,surname,salary,doublSalare));
    }


}

package Shapes;

public class Triangle extends ShapesClas {

    private double sideA;
    private double sideB;
    private double sideC;

    public Triangle(double inA, double inB, double inC){
        sideA = inA;
        sideB = inB;
        sideC = inC;
    }

    @Override
    double calculateArea() {
       double semiperimeter = (sideA+sideB+sideC)/2;
       return Math.sqrt(semiperimeter*(semiperimeter-sideA)*(semiperimeter-sideB)*(semiperimeter-sideC));
    }

    @Override
    double calculatePerimeter() {
        return sideA+sideB+sideC;
    }

    @Override
    void propertiesShapes() {
        System.out.println("with side a="+String.format("%.2f",sideA)+" side b="+String.format("%.2f",sideB)+" side c="+String.format("%.2f",sideC));
    }
}

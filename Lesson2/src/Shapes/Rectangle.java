package Shapes;

public class Rectangle extends ShapesClas {

    private double width;
    private double length;
    private double diagonal;
    private Shapes shapes;

    public Rectangle(double inWidth, double inLength, Shapes inShapes){
        width = inWidth;
        length = inLength;
        diagonal = Math.sqrt(Math.pow(inWidth,2)+Math.pow(inLength,2));
        shapes = inShapes;
    }

    @Override
    double calculateArea() {
        return width*length;
    }

    @Override
    double calculatePerimeter() {
        return 2*(width+length);
    }

    @Override
    void propertiesShapes() {
        if (shapes == Shapes.RECTANGLE) System.out.println("with width="+String.format("%.2f",width)+" length="+String.format("%.2f",length));
        else System.out.println("with side="+String.format("%.2f",width));
    }

    @Override
    public void showAreaAndPerimeter(Shapes inShapes, boolean choosedShapes) {
        super.showAreaAndPerimeter(inShapes, true);

        Triangle rectangleOfTriangles = new Triangle(width,length,diagonal);
        System.out.println("The shapes consists of two triangles");
        rectangleOfTriangles.showAreaAndPerimeter(Shapes.TRIANGLE, false);
    }
}

package Shapes;
import java.util.Scanner;

public class WorkTheShapes {

    public static void main(String[] args) {

        System.out.println("Please enter title shapes (CIRCLE, SQUARE, RECTANGLE, TRIANGLE):");
        String question;
        Scanner sc = new Scanner(System.in);
        do {
             question = sc.next();
        }
        while (!elementMatch(question));

        Shapes enterShapes = Shapes.valueOf(question);

        switch (enterShapes){
            case CIRCLE:
                System.out.println("Please enter radius (cm)");
                double inRadius = sc.nextDouble();
                Circle myCircle = new Circle(inRadius);
                myCircle.showAreaAndPerimeter(enterShapes,true);
                break;
            case TRIANGLE:
                System.out.println("Please enter side A (cm)");
                double inSideA = sc.nextDouble();
                System.out.println("Please enter side B (cm)");
                double inSideB = sc.nextDouble();
                System.out.println("Please enter side C (cm)");
                double inSideC = sc.nextDouble();
                Triangle myTriangle = new Triangle(inSideA,inSideB,inSideC);
                myTriangle.showAreaAndPerimeter(enterShapes,true);
                break;
            case RECTANGLE:
                System.out.println("Please enter width (cm)");
                double inwidth = sc.nextDouble();
                System.out.println("Please enter length (cm)");
                double inlength = sc.nextDouble();
                Rectangle myRectangle = new Rectangle(inwidth, inlength,Shapes.RECTANGLE);
                myRectangle.showAreaAndPerimeter(enterShapes,true);
                break;
            case SQUARE:
                System.out.println("Please enter side (cm)");
                double inSide= sc.nextDouble();
                Rectangle mySQUARE = new Rectangle(inSide, inSide,Shapes.SQUARE);
                mySQUARE.showAreaAndPerimeter(enterShapes,true);
                break;
        }

    }

    private static boolean elementMatch(String inNameElement){
        try {
            Shapes.valueOf(inNameElement);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

}

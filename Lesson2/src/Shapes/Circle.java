package Shapes;

public class Circle extends ShapesClas {

    private double radius;


    public Circle (double inRadius){
        radius = inRadius;
    }

    @Override
    public double calculateArea() {
        return 3.1415*Math.pow(radius,2);
    }

    @Override
    public double calculatePerimeter() {
        return 2*3.1415*radius;
    }

    @Override
    void propertiesShapes() {
        System.out.println("with radius = "+String.format("%.2f",radius));
    }
}

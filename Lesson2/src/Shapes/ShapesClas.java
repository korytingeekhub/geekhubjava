package Shapes;

public abstract class ShapesClas {

    abstract double calculateArea();
    abstract double calculatePerimeter();
    abstract void propertiesShapes();

    public void showAreaAndPerimeter(Shapes inShapes, boolean choosedShapes)
    {
        if (choosedShapes) System.out.println("You choosed "+inShapes);
        propertiesShapes();
        System.out.println("Area = "+String.format("%.2f",calculateArea()));
        System.out.println("Perimeter = "+String.format("%.2f",calculatePerimeter()));

    }

}

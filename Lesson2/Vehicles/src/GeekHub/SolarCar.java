package GeekHub;

public class SolarCar extends Car {

    public SolarCar(int inMaxPower) {
        super(inMaxPower, 1);
        setFuelConsumption(0.0);
    }

    @Override
    public void accelerate() {
        int curentPower = getEngine();
        super.accelerate();
        if (getEngine() > curentPower) System.out.println("The car accelerated");
        else System.out.println("You have max accelerated");
    }

    @Override
    public void brake() {
        super.brake();
        if (getWheels() > 0) System.out.println("The car brake");
        else System.out.println("The car stopped");
    }

    @Override
    public void turn() {
        if (getWheels() == 0) {
            System.out.println("The car wheels do not rotate");
        }
        else {
            super.turn();
            System.out.println("The car turn, but power decreased");
        }
    }

    @Override
    void carPanel() {
        System.out.println("AC power = "+getEngine()+" Turns of wheels = "+getWheels());
    }
}

package GeekHub;

public class TestDrive {

    public static void main(String[] args) {

        System.out.println("**************Test drive car*************");
        PassengerCar myCar = new PassengerCar(120,5, 0.5);
        for (int i = 0; i < 5; i++) {
            myCar.accelerate();
        }
        for (int i = 0; i < 4; i++) {
            myCar.brake();
        }
        myCar.turn();
        myCar.turn();
        myCar.brake();
        myCar.accelerate();

        System.out.println("**************Test boat*************");
        Boat myBoat = new Boat(20,2,0.2);
        for (int i = 0; i < 5; i++){
            myBoat.accelerate();
        }
        myBoat.doubleAccelerate();
        myBoat.turn();
        myBoat.brake();
        myBoat.accelerate();

        System.out.println("**************Test solar car*************");
        SolarCar mySolarCar = new SolarCar(90);
        for (int i = 0; i < 10; i++){
            mySolarCar.accelerate();
        }
        for (int i = 0; i < 4; i++) {
            mySolarCar.brake();
        }
        mySolarCar.turn();
        mySolarCar.turn();
        mySolarCar.brake();
        mySolarCar.accelerate();


    }


}

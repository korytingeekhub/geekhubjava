package GeekHub;

public interface Driveable {

    public void accelerate();
    public void brake();
    public void turn();
}

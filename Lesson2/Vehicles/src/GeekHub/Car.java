package GeekHub;

public abstract class Car implements Driveable  {

    private int engine;
    private int wheels;
    private double gasTank;
    private int maxPower;

    private double fuelConsumption;

    public int getEngine() {

        return engine;
    }

    public int getWheels() {
        return wheels; }

    public double getGasTank() {
        return gasTank;
    }

    public double getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(double fuelConsumption) {

        this.fuelConsumption = fuelConsumption;
    }

    public Car(int inMaxPower, int inGasTank)
    {
        maxPower = inMaxPower;
        wheels = 0;
        gasTank = inGasTank;
        engine = 0;
    }

    @Override
    public void accelerate() {

        plusPower();
    }

    @Override
    public void brake(){

        minusPower();
    }

    @Override
    public void turn() {
        minusPower();
    }

    private void plusPower()
    {
        if (gasTank >= fuelConsumption) {
            if (maxPower > engine) engine++;
            gasTank -= fuelConsumption;
            wheels++;
            carPanel();
        }
        else
        {
            minusPower();
        }
    }

    private void minusPower(){

        if (engine > 0)
        {
            engine--;
            wheels--;
            if (gasTank >= fuelConsumption) gasTank -= fuelConsumption;
        }
        carPanel();
    }

     abstract void carPanel();


}

package GeekHub;

public class Boat extends Car {


    public Boat(int inMaxPower, int inGasTank, double inFuelConsumption) {
        super(inMaxPower, inGasTank);
        setFuelConsumption(inFuelConsumption);
    }


    @Override
    public void accelerate() {
        int curentPower = getEngine();
        super.accelerate();
        if (getEngine() > curentPower) System.out.println("The boat accelerated");
        else System.out.println("Error. Look at boat control panel");
    }

    @Override
    public void brake() {
        super.brake();
        if (getWheels() > 0) System.out.println("The boat propeller brake");
        else System.out.println("The boat propeller stopped");
    }

    @Override
    public void turn() {
        super.turn();
        System.out.println("The boat turn");
    }

    public void doubleAccelerate(){
        setFuelConsumption(getFuelConsumption()*2);
        System.out.println("Fuel consumption increased");
        this.accelerate();
        this.accelerate();
        setFuelConsumption(getFuelConsumption()/2);
        System.out.println("Fuel consumption diminished");
    }

    @Override
    void carPanel() {
        System.out.println("AC power = "+getEngine()+" Propeller power = "+getWheels()+" Fuel balance = "+String.format("%.2f",getGasTank()));
    }
}
